year = ""

while year.isdigit() == False:
	year = input("Please input a year: \n")

	if year.isdigit() == False or int(year) < 1:
		print("Please enter valid year.")
	else:
		if int(year)%4 == 0:
			print(f"{year} is a leap year.")
			break
		else:
			print(f"{year} is not a leap year")
			break

row = int(input(f"Enter number of rows: \n "))
column = int(input(f"Enter number of column: \n "))
ast = ""

for x in range(row):
	for y in range(column):
		ast = ast + "*"
	print(ast)
	ast = ""